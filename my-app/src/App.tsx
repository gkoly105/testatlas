import React from 'react';
import logo from './logo.svg';
import './App.css';
import {INewsItem} from "./models/INewsItem";
import News from "./components/News";
import 'semantic-ui-css/semantic.min.css';
import {Grid} from "semantic-ui-react";
import Filter from "./components/Filter";
import {emptyFilter, filterNews, INewsFilter} from "./models/INewsFilter";
import {BrowserRouter} from "react-router-dom";


function App() {
    // Полный список новостей
    const [news, setNews] = React.useState<INewsItem[]>([]);

    //Применяемый фильтр
    const [filter, setFilter] = React.useState<INewsFilter>(emptyFilter);

    // Получение данных с сервера
    const fetchNews = () => {
        fetch('https://gist.githubusercontent.com/SlivTime/7e259e2b8c3206163295cbf1ab01a5f7/raw/1146ffc9aeb46e6c0b62c21bf4209ba245600398/events.json')
        .then((resp) => {
            return resp.json();
        })
        .then((res: INewsItem[]) => {
            res = res.map(item => {
                item.date = new Date(item.date);
                return item;
            });
            setNews(res);
        })
    };

    // Получение новостей при открытие страницы
    React.useEffect(() => {
        setTimeout(() => fetchNews(), 1);
    }, []);


    console.log('news', news)

    // Фильтрация
    const filteredNews = filterNews(news, filter);
    console.log('filtered', filteredNews)

    return (
        <BrowserRouter>
            <div className="App">
                <Grid textAlign={'left'}>
                    <Grid.Row>
                        <Grid.Column width={5}>
                            {/* Форма фильтра */}
                            <Filter sendFilter={setFilter}/>
                        </Grid.Column>
                        <Grid.Column width={11}>
                            {/* Новости */}
                            <News data={filteredNews}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        </BrowserRouter>
    );
}

export default App;
