import React, {useEffect} from 'react';
import {emptyFilter, getFilterFromQuery, INewsFilter, setQueryFromFilter, SortType} from "../models/INewsFilter";
import {Button, Form, Label} from 'semantic-ui-react';
import "react-datepicker/dist/react-datepicker.css";
import ReactDatePicker from "react-datepicker";
import {useHistory, useLocation} from "react-router";

// Пропсы формы фильтра
interface IFilterProps {
    sendFilter: ((filter: INewsFilter) => void);
}

// Варианты сортировки
const sortOptions = [
    {key: 'a', value: SortType.Date, text: 'По убыванию даты'},
    {key: 'b', value: SortType.Comments, text: 'По убыванию количества комментариев'},
];

// Форма фильтра
const Filter: React.FC<IFilterProps> = ({sendFilter}) => {
    // Текущее значение фильтра
    const [filter, setFilter] = React.useState<INewsFilter>(emptyFilter);

    // Обработчики изменений
    const onChangeText = (text: string) => {
        let newFilter = Object.assign({}, filter, {title: text});
        setFilter(newFilter);
    };

    const onChangeDateSince = (date: Date) => {
        let newFilter = Object.assign({}, filter, {dateSince: date});
        setFilter(newFilter);
    };

    const onChangeDateTill = (date: Date) => {
        let newFilter = Object.assign({}, filter, {dateTill: date});
        setFilter(newFilter);
    };

    const onChangeSortType = (type: SortType) => {
        let newFilter = Object.assign({}, filter, {sortType: type});
        setFilter(newFilter);
    };

    const history = useHistory();

    // Эффект установки параметров url из фильтра
    useEffect(() => {
        let params = new URLSearchParams();
        setQueryFromFilter(filter, params);
        history.push({
            pathname: '/',
            search: "?" + String(params),
        })
    }, [filter]);

    const location = useLocation();

    // Эффект получения фильтра из параметров url
    useEffect(() => {
        const query = new URLSearchParams(location.search);
        let newFilter = getFilterFromQuery(query);
        setFilter(newFilter);
        sendFilter(newFilter);
    }, []);

    return (
        <div>
            <Form>
                <Form.Input value={filter.title}
                            onChange={(e) => onChangeText(e.target.value)}
                            fluid label='Текст'
                            placeholder='текст'/>
                <Form.Group>
                    <Label>с</Label>
                    <ReactDatePicker
                        selected={filter.dateSince}
                        onChange={onChangeDateSince}
                    />

                </Form.Group>
                <Form.Group>
                    <Label>до</Label>
                    <ReactDatePicker
                        selected={filter.dateTill}
                        onChange={onChangeDateTill}
                    />

                </Form.Group>
                <Form.Select value={filter.sortType}
                             options={sortOptions}
                             onChange={(e, data) => onChangeSortType(data.value as SortType)}
                             placeholder='Сортировка'/>
            </Form>
            <Button onClick={() => sendFilter(filter)}>Найти</Button>
        </div>
    );
}

export default Filter;
