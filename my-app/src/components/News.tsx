import React from 'react';
import {INewsItem} from "../models/INewsItem";
import NewsItem from "./NewsItem";

// Пропсы списка нвостей
interface INewsProps {
    data: INewsItem[];
}

const News: React.FC<INewsProps> = ({data}) =>{

    return (
        <div>
            {data.map((item)=>(
                <div key={item.id}>
                    <NewsItem data={item}/>
                </div>
            ))}
        </div>
    );
}

export default News;
