import * as React from 'react';
import {INewsItem} from "../models/INewsItem";
import {Card} from "semantic-ui-react";

// Пропсы элемента новости
interface INewsItemProps {
    data: INewsItem;
}


const NewsItem: React.FC<INewsItemProps> = ({data}) =>{

    return (
        <div>
            <Card fluid={true}>
                <Card.Content header={data.date.toLocaleDateString()} />
                <Card.Content description={data.title} />
                <Card.Content extra={true}>
                    Количество комментариев: {data.comments}
                </Card.Content>
            </Card>
        </div>
    );
}

export default NewsItem;
