import {INewsItem} from "./INewsItem";

// Тип сортировки
export enum SortType {
    Date = 'date',
    Comments = 'comments',
}

// Тип фильтра
export interface INewsFilter {
    sortType: SortType;
    title: string;
    dateSince: Date | undefined;
    dateTill: Date | undefined;
}

// Пустой фильтр
export const emptyFilter : INewsFilter = {
    sortType: SortType.Date,
    title: '',
    dateSince: undefined,
    dateTill: undefined,
}

// Функция фильтрации и сортировки
export function filterNews (news: INewsItem[], filter: INewsFilter) : INewsItem[] {
    return news.filter(item => {
        if (filter.title && !item.title.includes(filter.title))
            return false;
        if (filter.dateSince && item.date.getTime() < filter.dateSince.getTime())
            return false;
        if (filter.dateTill && item.date.getTime() > filter.dateTill.getTime())
            return false;
        return true;

    }).sort((a, b) => {
        if (filter.sortType == SortType.Comments) {
            return b.comments - a.comments;
        }
        else {
            return b.date.getTime() - a.date.getTime();
        }
    })
}

// Перевод из параметров url в фильтр
export function getFilterFromQuery(query: URLSearchParams) : INewsFilter {
    const filter: INewsFilter = Object.assign({}, emptyFilter);
    filter.title = query.get('title') || '';
    filter.sortType = (query.get('sortType') as SortType || SortType.Date);
    let dateSince = query.get('dateSince') || undefined;
    if (dateSince) filter.dateSince = new Date(dateSince);
    let dateTill = query.get('dateTill') || undefined;
    if (dateTill) filter.dateTill = new Date(dateTill);

    return filter;
}

// Перевод из фильтра в параметры url
export function setQueryFromFilter(filter: INewsFilter, params: URLSearchParams) : void {
    params.append('title', filter.title)
    params.append('sortType', filter.sortType)
    filter.dateSince && params.append('dateSince', filter.dateSince.toISOString())
    filter.dateTill && params.append('dateTill', filter.dateTill.toISOString())
}
