// Тип новости
export interface INewsItem {
    id: number;
    title: string;
    comments: number;
    date: Date;
}
